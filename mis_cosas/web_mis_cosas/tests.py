from django.test import TestCase
from .models import Alimentador, Item, Usuario, Comentario, Voto
from django.contrib.auth.models import User

# Create your tests here.

class TestAlimentador(TestCase):

    def setUp(self):
        self.id_canal = 'Music for Sleep, Meditation and Healing'
        self.titulo = 'UCO32kTV44iC3dWt2UmSNWIg'
        self.enlace = 'https://www.youtube.com/channel/'

    def test_crear_alimentador(self):
        alimentador = Alimentador(id_canal=self.id_canal, titulo=self.titulo, enlace=self.enlace)
        alimentador.save()



class TestItem(TestCase):

    def setUp(self):
        self.id_video = 'HZOodD84MR8'
        self.titulo = 'UCO32kTV44iC3dWt2UmSNWIg'
        self.enlace = 'https://www.youtube.com/watch?v=HZOodD84MR8'

        id_canal = "Music for Sleep, Meditation and Healing"
        titulo = 'UCO32kTV44iC3dWt2UmSNWIg'
        enlace = 'https://www.youtube.com/channel/'
        alimentador = Alimentador(id_canal=id_canal, titulo=titulo, enlace=enlace)
        alimentador.save()
        self.alimentador = alimentador
