# Generated by Django 3.0.6 on 2020-06-13 11:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web_mis_cosas', '0007_voto_item'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='bg_color',
            field=models.CharField(default='white', max_length=20),
        ),
        migrations.AddField(
            model_name='usuario',
            name='letras_size',
            field=models.CharField(default='pequeña', max_length=10),
        ),
    ]
