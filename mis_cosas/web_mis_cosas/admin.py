from django.contrib import admin

# Register your models here.

from django.contrib import admin
from .models import Post, Usuario, Item, Alimentador, Comentario

admin.site.register(Post)
admin.site.register(Usuario)
admin.site.register(Item)
admin.site.register(Alimentador)
admin.site.register(Comentario)
