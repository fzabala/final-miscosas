from django.apps import AppConfig


class WebMisCosasConfig(AppConfig):
    name = 'web_mis_cosas'
