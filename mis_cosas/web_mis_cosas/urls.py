from django.urls import path
from django.contrib.auth import views as views_login
from . import views

urlpatterns = [
    path('', views.principal_page, name='principal_page'),
    path('item/', views.item_page, name='item_page'),
    path('item/<str:id_vid>', views.item_page, name='item_page'),
    path('registrarse/', views.registrarse_page, name='registrarse_page'),
    path('post/new/', views.post_new, name='post_new'),
    path('informacion/', views.informacion_page, name='informacion_page'),
    path('alimentadores/', views.alimentadores, name='alimentadores'),
    path('alimentadores/<str:alim_id>', views.alimentador_info, name='alimentador_info'),
#    path('alimentadores/', views.alimentadores_page, name='alimentadores_page'),
    path('usuarios/', views.usuarios_page, name='usuarios_page'),
    path('usuarios/<str:uname>', views.usuario_page, name='usuario_page'),
    path('colores', views.colores, name='colores'),
#    path('login', views_login.LoginView.as_view(), name='login_user'),
    path('login', views.login_user, name='login_user'),
    path('logout/', views.logout_user, name='logout_user'),
]
