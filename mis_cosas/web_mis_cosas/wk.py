from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Alimentador, Item

import sys
import string

class WikiHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.content = ""
        self.arttitle = ""
        self.artlink = ""
        self.nitems = 0

        self.title = ""
        self.link = ""
        self.autor = ""
        self.date = ""
        self.chlink = ""
        self.article = ""
        self.item = []

    def startElement (self, name, attrs):

        if name == 'title' and name != self.title:
            self.inContent = True
        elif name == 'link':
            self.inContent = True
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'pubDate':
                self.inContent = True
            elif name == 'dc:creator':
                self.inContent = True


    def endElement (self, name):

        if self.inItem == False:
            if name == 'title':
                self.arttitle = self.content
                self.content = ""
                self.inContent = False
            elif name == 'link':
                self.artlink = self.content
                self.content = ""
                self.inContent = False

        if name == 'item':
            self.inItem = False
#            try:
#                article = Alimentador.objects.get(titulo = self.arttitle)
#
#            except Alimentador.DoesNotExist:
#                article = Alimentador(titulo = self.arttitle, link = self.artlink, id = self.arttitle, num_items=0, puntuacion=0)
#
#            self.article = article
#            article.save()

#            try:
#                item = Item.objects.get(link = self.link)
#                self.item.append(item)
#            except Item.DoesNotExist:
#            item = Item(alimentador = self.article, titulo = self.title, link = self.link, id=self.autor,
#                             descripcion = self.date, likes = 0, dislikes = 0)
#                item.save()
#                article.num_items = article.num_items + 1

            self.nitems = self.nitems + 1 # Personalizada
            self.item.append({'link': self.artlink,
                                'title': self.arttitle,
                                 'link_cambio': self.link,
                                 'pubDate': self.date,
                                 'dc:creator': self.autor,
                                 'comments': self.chlink})

#            self.item.append(item)

        elif self.inItem:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'link':
                self.link = self.content
                self.content = ""
                self.inContent = False
            elif name == 'pubDate':
                self.date = self.content
                self.content = ""
                self.inContent = False
            elif name == 'dc:creator':
                self.autor = self.content
                self.content = ""
                self.inContent = False
            elif name == 'comments':
                self.chlink = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class WikiArticle:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WikiHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def cambios (self):
        return self.handler.item

    def arttitle (self):
        return self.handler.arttitle

    def artlink (self):
        return self.handler.artlink

    def nitems (self):
        return self.handler.nitems

    def item (self):
        return self.handler.item

    def link (self):
        return self.handler.link

    def chlink (self):
        return self.handler.chlink
