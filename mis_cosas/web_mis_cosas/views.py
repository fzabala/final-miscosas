from django.shortcuts import render
from django.utils import timezone
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import login, logout, authenticate, get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.template.loader import get_template

from urllib.request import urlopen

from .models import Post, Alimentador, Item, Usuario, Comentario, Voto
from .forms import PostForm, LoginForm, UserForm #, YoutubeForm
from .yt import YTChannel
from .wk import WikiArticle

import urllib



def principal_page(request):



    if request.method == "GET":

       #Lista de 10 items mas votados
        lista_items = []
        items = Item.objects.all()
        for item in items:
            puntuacion = item.votos_pos - item.votos_neg
            if len(lista_items) < 10:
                lista_items.append(item)
            else:
                caja = item
                n = 0
                for e in lista_items:
                    e_puntuacion = e.votos_pos - e.votos_neg
                    if e_puntuacion < puntuacion:
                        tmp = lista_items[n]
                        lista_items[n] = caja
                        caja = tmp
                    n = n + 1

        posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
        form = AuthenticationForm()
        lista_alimentadores = Alimentador.objects.all()

        lista_votos = []
        usuario = request.user.username
        if usuario:
            usuario = Usuario.objects.get(username=usuario)
            lista_votos = Voto.objects.all().filter(autor=usuario)
        #    print(lista_votos.count())
            if lista_votos.count() > 5:
                lista_votos = lista_votos[::-1]
                lista_votos = lista_votos[0:5]

        formato = request.GET.get('format')
        if formato and formato == 'xml':
            return render(request, 'blog/principal.xml', {'form': form, 'lista_items':lista_items, 'lista_alimentadores':lista_alimentadores, 'lista_votos':lista_votos, 'usuario':usuario}, content_type="application/xml")
        elif formato and formato == 'json':
            return render(request, 'blog/principal.json', {'form': form, 'lista_items':lista_items, 'lista_alimentadores':lista_alimentadores, 'lista_votos':lista_votos, 'usuario':usuario}, content_type="application/json")
        else:
            return render(request, 'blog/principal.html', {'form': form, 'lista_items':lista_items, 'lista_alimentadores':lista_alimentadores, 'lista_votos':lista_votos, 'usuario':usuario})

    else:
        id_or_name = request.POST['valor']

        if not id_or_name or id_or_name[0] == " ":
            return render(request, 'blog/void.html')
        else:

            action = request.POST['action']

            if action == "VER_YT":
                xmlStream = urlopen('https://www.youtube.com/feeds/videos.xml?channel_id=' + id_or_name)
                link = ('https://www.youtube.com/channel/' + id_or_name)
                channel = YTChannel(xmlStream)

                try:
                    alimentador = Alimentador.objects.get(enlace=channel.uriAlim())
                except Alimentador.DoesNotExist:
                    alimentador = Alimentador(titulo= channel.nameAlim(),
                                  enlace=channel.uriAlim(), id_canal = id_or_name)
                    alimentador.save()

                for video in channel.videos():
                    video = Item(alimentador = alimentador, titulo=video['title'],
                                  enlace=video['link'], descripcion = video['description'], id_video = video['id'], tipo = "youtube")
                    video.save()

            if action == "VER_WK":
                xmlStream = urlopen('https://es.wikipedia.org/w/index.php?title=' + id_or_name + '&action=history&feed=rss')
                link = ('https://es.wikipedia.org/wiki/' + id_or_name)
                articulo = WikiArticle(xmlStream)

                try:
                    alimentador = Alimentador.objects.get(titulo=articulo.arttitle())
                except Alimentador.DoesNotExist:
                    alimentador = Alimentador(titulo = articulo.arttitle(), enlace = articulo.artlink(), id_canal = articulo.arttitle())
                    alimentador.save()


                for cambio in articulo.cambios():
                    n = cambio['link_cambio'].find("index.php?") + 10
                    cambio_id = cambio['link_cambio'][n:]
                    tit = cambio['title'] + " | " + cambio['link_cambio'][n:]
                    print(tit)

                    cambio = Item(alimentador = alimentador, titulo=tit,
                                  enlace=cambio['link_cambio'], descripcion = cambio['comments'], id_video = cambio_id, tipo = "wikipedia")

                    cambio.save()


#Ahora muestra todos los alimentadores pero tendrá que mostrar los que correspondad (Youtube o Wikipedia)
        lista_alimentadores = Alimentador.objects.all()
        contexto = {'lista_alimentadores': lista_alimentadores}
        return render(request, 'blog/alimentadores.html', contexto)


def alimentadores(request):
    lista_alimentadores = Alimentador.objects.all()
    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)


    return render(request, 'blog/alimentadores.html', {'lista_alimentadores': lista_alimentadores, 'usuario': usuario})


def alimentador_info(request, alim_id):
    try:
        alimentador = Alimentador.objects.get(id_canal = alim_id)
    except Alimentador.DoesNotExist:
        return render(request, 'blog/void.html')

    if request.method == "POST":
        elegir = request.POST.get('elegir', False)

        if elegir == "Eliminar":
            alimentador.elegido = False
            alimentador.save()

        if elegir == "Elegir":
            alimentador.elegido = True
            alimentador.save()

    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)


    context = {'alimentador': alimentador, 'usuario': usuario}
    return render(request, 'blog/alimentador.html', context)


def item_page(request, id_vid):

    item = Item.objects.get(id_video = id_vid)
    alimentador = item.alimentador
    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)


    if request.method == "POST":
        action = request.POST.get('action', False)
        voto = request.POST.get('voto', False)

        lista_items_votados = Voto.objects.all().filter(autor=usuario)


        if action == "Comentar":
            texto_comentario = request.POST['valor']
            autor_comentario = request.user.username
            autor_comentario = Usuario.objects.get(username=autor_comentario)
            item_comentado = Item.objects.get(id_video = id_vid)
            comentario = Comentario(autor = autor_comentario, texto = texto_comentario, item = item_comentado)
            comentario.save()

        elif voto == "( + )":
            if not lista_items_votados.filter(item = item).exists():
                item.votos_pos = item.votos_pos + 1
                alimentador.votos_pos = alimentador.votos_pos + 1
                alimentador.puntuacion = alimentador.puntuacion + 1
                item.save()
                alimentador.save()

                autor_voto = request.user.username
                autor_voto = Usuario.objects.get(username=autor_voto)
                item_votado = Item.objects.get(id_video = id_vid)
                new_voto = Voto(autor = autor_voto, item = item_votado, tipo = "Positivo")
                new_voto.save()
            else:
                autor_voto = request.user.username
                autor_voto = Usuario.objects.get(username=autor_voto)
                item_votado = Item.objects.get(id_video = id_vid)
                votos = Voto.objects.filter(autor = autor_voto)
                for v in votos:
                    if v.item == item_votado and v.tipo == "Negativo":
                        v.tipo = "Positivo"
                        v.save()

                        item.votos_neg = item.votos_neg - 1
                        item.votos_pos = item.votos_pos + 1
                        alimentador.votos_neg = alimentador.votos_neg - 1
                        alimentador.votos_pos = alimentador.votos_pos + 1
                        alimentador.puntuacion = alimentador.puntuacion + 2
                        item.save()
                        alimentador.save()


                print("AHORA###################")


        elif voto == "( - )":
            if not lista_items_votados.filter(item = item).exists():
                item.votos_neg = item.votos_neg + 1
                alimentador.votos_neg = alimentador.votos_neg + 1
                alimentador.puntuacion = alimentador.puntuacion - 1
                item.save()
                alimentador.save()

                autor_voto = request.user.username
                autor_voto = Usuario.objects.get(username=autor_voto)
                item_votado = Item.objects.get(id_video = id_vid)
                new_voto = Voto(autor = autor_voto, item = item_votado, tipo = "Negativo")
                new_voto.save()


            else:
                autor_voto = request.user.username
                autor_voto = Usuario.objects.get(username=autor_voto)
                item_votado = Item.objects.get(id_video = id_vid)
                votos = Voto.objects.filter(autor = autor_voto)
                for v in votos:
                    if v.item == item_votado and v.tipo == "Positivo":
                        v.tipo = "Negativo"
                        v.save()

                        item.votos_neg = item.votos_neg + 1
                        item.votos_pos = item.votos_pos - 1
                        alimentador.votos_neg = alimentador.votos_neg + 1
                        alimentador.votos_pos = alimentador.votos_pos - 1
                        alimentador.puntuacion = alimentador.puntuacion - 2
                        item.save()
                        alimentador.save()

                print("AHORA###################")


    form = AuthenticationForm()
    puntuacion = item.votos_pos - item.votos_neg
    return render(request, 'blog/item.html', {'form': form, 'item': item, 'puntuacion': puntuacion, 'usuario': usuario})


def post_new(request):
    form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


def registrarse_page(request):

    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)

    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            user = form.save()
            uname =  form.cleaned_data["username"]
            passwd = form.cleaned_data["password1"]
            usr = Usuario(username = uname, password = passwd)
            usr.save()

            if user is not None:
                do_login(request, user)
                return redirect('/registrarse')

        return redirect("/registrarse/")
    else:
        form = UserCreationForm()
        return render(request, 'blog/registrarse.html', {'form': form, 'usuario': usuario})


def login_user(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "users/login.html", {'form': form})


def logout_user(request):
    logout(request)
    message = "Esperamos verte pronto. Sigue navegando por la <a href="#">web</a>"
    return redirect("/")


def usuarios_page(request):
   # usuarios = Usuario.objects.all()
    user = get_user_model()
    users =user.objects.all()
    form = AuthenticationForm()

    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)

    return render(request, 'blog/usuarios.html', {'form': form, 'users': users, 'usuario': usuario})


def colores(request):

    usuario = request.user.username
    usuario = Usuario.objects.get(username=usuario)

    if request.method == 'POST':

        bg_color = usuario.bg_color
        bg = request.POST.get('bg', False)
        if bg == "ligero":
            bg_color = "white"
        elif bg == "oscuro":
            bg_color = "powderblue"

        letras_size = usuario.letras_size
        tletras = request.POST.get('tletras', False)
        if tletras == "pequeña":
            letras_size = 75
        elif tletras == "normal":
            letras_size = 100
        elif tletras == "grande":
            letras_size = 200

        usuario.bg_color = bg_color
        usuario.letras_size = letras_size
        usuario.save()



        comentarios = Comentario.objects.all().filter(autor=usuario)
        form = AuthenticationForm()
        lista_items_votados = Voto.objects.all().filter(autor=usuario)

        return render(request, 'blog/usuario.html', {'usuario': usuario, 'form': form, 'comentarios': comentarios, 'lista_items_votados': lista_items_votados, 'bg_color': bg_color})



def usuario_page(request, uname):

    usuario = Usuario.objects.get(username=uname)
    comentarios = Comentario.objects.all().filter(autor=usuario)
    form = AuthenticationForm()
    lista_items_votados = Voto.objects.all().filter(autor=usuario)

    return render(request, 'blog/usuario.html', {'usuario': usuario, 'form': form, 'comentarios': comentarios, 'lista_items_votados': lista_items_votados})



def informacion_page(request):
    form = AuthenticationForm()

    usuario = request.user.username
    if usuario:
        usuario = Usuario.objects.get(username=usuario)

    return render(request, 'blog/informacion.html', {'form': form, 'usuario': usuario})

