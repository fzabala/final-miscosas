# final-miscosas

Repositorio de inicio de la práctica final. Recuerda realizar una derivación (fork) de este repositorio y hacer múltiples commits al realizar tu práctica.

Fork realizado

Felipe Zabala Sánchez

GitLab con el código:
https://gitlab.etsit.urjc.es/fzabala/final-miscosas/

Web desplegada en:
http://fzabala.pythonanywhere.com/

Link vídeo Youtube, parte obligatoria:
https://youtu.be/nh1XROYiCDA

Usuarios disponibles:
user1:Test.1234.
user2:Test.1234.
user3:Test.1234.
admin:Test.1234.


Parte obligatoria:
Se cubren todas las partes de la funcionalidad mínima (parte obligatoria). Quedó medio pendiente, por falta de tiempo, completar el fichero con los tests (tests.py) y añadir fotos de perfil a los usuarios.

Parte optativa:
Inclusión del favicon
Utilización de javascript
Un diseño fantástico y poco cargado

Link vídeo Youtube, parte optativa:
https://youtu.be/L0BESkqa3wg
